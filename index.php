<?php

/**
 * Gets data from the "database".
 *
 * @return array
 */
function get_users() {
	// Don't touch this method. This just mocks a possible response.
	return [
		[
			'name'    => 'Helen Abraham',
			'email'   => 'helen.a@example.com',
			'website' => '<http://helenabraham.example.com>',
		],
		[
			'name'    => 'Rashid Mihyar Quraishi',
			'email'   => 'rashid.m.q@example.com',
			'website' => 'rashid.example.com',
		],
		// etc.
	];
}

function get_user_websites() {
	$users = get_users();
	$websites = [];

	// Collect list of websites users have entered on their profile.
	foreach ( $users as $user ) {
		if ( is_invalid_url( $user['website'] )) {
			continue;
		} else {
			$websites[] = $user['website'];
		}
	}

	return $websites;
}

function is_invalid_url( $url ){
    return empty( $url ) || ! filter_var( $url, FILTER_VALIDATE_URL ) ? true : false;
}

function test_it_gets_user_websites() {
	$websites = get_user_websites();

	// Assume `assertTrue()` is provided by the framework.
	// The first param needs to be true, the second is the error if it's not.
	// Feel free to make up similar test functions like "assertEquals" or "assertContains" (or refer to PHPUnit's docs if you like).
	assertTrue( count( $websites ), 'Must find a list of websites entered by users.' );
}