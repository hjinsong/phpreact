import React from "react";
import { websites } from "./websites";

const App = () => {
    const [list, setList] = React.useState(websites);

    function handleRemove(email) {
      const newList = list.filter((item) => item.email !== email);
  
      setList(newList);
    }
  
    return (
      <ul>
        {list.map((item,key) => (
          <li key={item.email}>
            <span>{item.website}</span>
            <button type="button" onClick={() => handleRemove(item.email)}>
              Remove
            </button>
          </li>
        ))}
      </ul>
    );
};

export default App;